package com.fpapps.legendchat.attachment.view;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.fpapps.legendchat.R;
import com.squareup.picasso.Picasso;


public class AttachmentDetailsActivity extends AppCompatActivity {
    public static final String EXTRA_ATTACHMENT_TITLE = "extra_attachment_title";
    public static final String EXTRA_ATTACHMENT_URL = "extra_attachment_url";
    public static final String EXTRA_ATTACHMENT_USER_NAME = "extra_attachment_user_name";
    public static final String EXTRA_MESSAGE_ID = "extra_message_id";

    private int mMessageId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attachment_details);

        ImageView mAttachmentImage = findViewById(R.id.iv_attachment_image);
        TextView attachmentTitle = findViewById(R.id.tv_attachment_title);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        String title = getIntent().getStringExtra(EXTRA_ATTACHMENT_TITLE);
        String url = getIntent().getStringExtra(EXTRA_ATTACHMENT_URL);
        String userName = getIntent().getStringExtra(EXTRA_ATTACHMENT_USER_NAME);
        mMessageId = getIntent().getIntExtra(EXTRA_MESSAGE_ID, -1);

        setTitle(userName);
        attachmentTitle.setText(title);

        //load the attachment image
        Picasso.get().load(url).into(mAttachmentImage);
    }

    private void showConfirmationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.confirm_delete_message)
                .setPositiveButton(R.string.yes, (dialogInterface, i) -> deleteObject())
                .setNegativeButton(R.string.no, null)
                .show();
    }

    private void deleteObject() {
        Intent dataIntent = new Intent();
        dataIntent.putExtra(EXTRA_MESSAGE_ID, mMessageId);
        setResult(RESULT_OK, dataIntent);
        supportFinishAfterTransition();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_attachment_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
            case R.id.delete:
                showConfirmationDialog();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
