package com.fpapps.legendchat.chat.model.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestPojo {
    @JsonProperty("messages")
    public List<MessagePojo> messages;

    @JsonProperty("users")
    public List<UserPojo> users;

}
