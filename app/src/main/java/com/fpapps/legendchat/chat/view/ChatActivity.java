package com.fpapps.legendchat.chat.view;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.fpapps.legendchat.R;
import com.fpapps.legendchat.attachment.view.AttachmentDetailsActivity;
import com.fpapps.legendchat.chat.adapters.ChatRecyclerViewAdapter;
import com.fpapps.legendchat.chat.model.pojo.AttachmentPojo;
import com.fpapps.legendchat.chat.model.pojo.MessageAndUser;
import com.fpapps.legendchat.chat.model.repository.RequestRepository;
import com.fpapps.legendchat.chat.presenter.ChatPresenter;
import com.fpapps.legendchat.chat.view.contract.RequestViewPresenterContract;
import com.fpapps.legendchat.dagger.component.DaggerChatActivityComponent;
import com.fpapps.legendchat.dagger.module.ApplicationModule;
import com.fpapps.legendchat.dagger.module.ChatPresenterModule;
import com.fpapps.legendchat.database.ApplicationDatabase;
import com.fpapps.legendchat.views.recycler.EndlessRecyclerViewScrollListener;

import java.util.ArrayList;

import javax.inject.Inject;

public class ChatActivity extends AppCompatActivity implements RequestViewPresenterContract.View, ChatRecyclerViewAdapter.OnAttachmentClickedListener, ChatRecyclerViewAdapter.OnLongClickListener {
    public static final int ATTACHMENT_REQUEST_CODE = 1000;

    @Inject
    ApplicationDatabase appDatabase;

    @Inject
    ChatPresenter chatPresenter;

    private ChatRecyclerViewAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        handleInjection();

        RecyclerView mRecyclerView = findViewById(R.id.rv_chat);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                chatPresenter.getMessages(page);
            }
        };
        mRecyclerView.addOnScrollListener(scrollListener);

        mAdapter = new ChatRecyclerViewAdapter(new ArrayList<>(), this, this);
        mRecyclerView.setAdapter(mAdapter);

        chatPresenter.getMessages(0);
    }

    /**
     * Handles dependency injection
     */
    private void handleInjection() {
        DaggerChatActivityComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .chatPresenterModule(new ChatPresenterModule(this))
                .build()
                .inject(this);
    }

    @Override
    public void getMessagesResult(ArrayList<MessageAndUser> messages) {
        mAdapter.setData(messages);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void getMessagesError(int errorType) {
        if (errorType == RequestRepository.ERROR_LOADING_FROM_NETWORK) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.no_internet_tittle)
                    .setMessage(R.string.no_internet_message)
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.try_again), (dialogInterface, i) -> {
                        chatPresenter.getMessages(0);
                        dialogInterface.dismiss();
                    })
                    .show();
        }
    }

    @Override
    public void notifyAdapterItemRemoved(int messageId) {
        mAdapter.removeItemFromDataset(messageId);
    }

    @Override
    public void onAttachmentClicked(String userName, int messageId, AttachmentPojo attachment, ImageView view) {
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                this,
                view,
                getString(R.string.attachment_transition));

        Intent intent = new Intent(this, AttachmentDetailsActivity.class);
        intent.putExtra(AttachmentDetailsActivity.EXTRA_ATTACHMENT_TITLE, attachment.title);
        intent.putExtra(AttachmentDetailsActivity.EXTRA_ATTACHMENT_URL, attachment.url);
        intent.putExtra(AttachmentDetailsActivity.EXTRA_ATTACHMENT_USER_NAME, userName);
        intent.putExtra(AttachmentDetailsActivity.EXTRA_MESSAGE_ID, messageId);
        startActivityForResult(intent, ATTACHMENT_REQUEST_CODE, options.toBundle());
    }

    @Override
    public boolean onLongClickPerformedOnItem(int messageId, AttachmentPojo attachment, View item) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.confirm_delete_message)
                .setPositiveButton(R.string.yes, (dialogInterface, i) -> chatPresenter.deleteMessage(messageId))
                .setNegativeButton(R.string.no, null)
                .show();
        return false;
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == ATTACHMENT_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            int messageId = data.getIntExtra(AttachmentDetailsActivity.EXTRA_MESSAGE_ID, -1);

            if (messageId != -1) {
                chatPresenter.deleteMessage(messageId);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        chatPresenter.dispose();
    }
}
