package com.fpapps.legendchat.chat.model.pojo;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "user")
public class UserPojo {

    @PrimaryKey
    @ColumnInfo(name = "id")
    @JsonProperty("id")
    public int id;

    @ColumnInfo(name = "name")
    @JsonProperty("name")
    public String name;

    @ColumnInfo(name = "avatarId")
    @JsonProperty("avatarId")
    public String avatarId;
}
