package com.fpapps.legendchat.chat.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fpapps.legendchat.R;
import com.fpapps.legendchat.chat.model.pojo.AttachmentPojo;
import com.fpapps.legendchat.chat.model.pojo.MessageAndUser;
import com.fpapps.legendchat.views.custom.AttachmentsView;
import com.squareup.picasso.Picasso;

import java.util.List;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;

public class ChatRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<MessageAndUser> mMessagesAndUsers;

    private OnLongClickListener onLongClickListener;
    private OnAttachmentClickedListener onAttachmentClickedListener;

    private static final int ITEM_NULL = 0;
    private static final int ITEM_SENT_MESSAGE = 1;
    private static final int ITEM_RECEIVED_MESSAGE = 2;

    public ChatRecyclerViewAdapter(List<MessageAndUser> messagesAndUsers,  OnLongClickListener onLongClickListener, OnAttachmentClickedListener onAttachmentClickedListener) {
        this.mMessagesAndUsers = messagesAndUsers;

        this.onLongClickListener = onLongClickListener;
        this.onAttachmentClickedListener = onAttachmentClickedListener;
    }

    public interface OnAttachmentClickedListener {
        void onAttachmentClicked(String userName, int messageId, AttachmentPojo attachment, ImageView view);
    }

    public interface OnLongClickListener {
        boolean onLongClickPerformedOnItem(int messageId, AttachmentPojo attachment, View item);
    }

    public void setData(List<MessageAndUser> mMessagesAndUsers) {
        this.mMessagesAndUsers.addAll(mMessagesAndUsers);
    }

    public void removeItemFromDataset(int messageId) {
        int index = getMessageIndex(messageId);
        mMessagesAndUsers.remove(index);
        notifyItemRemoved(index);
    }

    private int getMessageIndex(int messageId) {
        for (int i = 0; i < mMessagesAndUsers.size(); i++) {
            MessageAndUser messageAndUser = mMessagesAndUsers.get(i);
            if (messageAndUser.message.id == messageId) {
                return i;
            }
        }
        return 0;
    }

    /**
     * Message = null => Loading state
     * Message user id = 1 => Sent message
     * Else Received message
     *
     * @param position position of view
     * @return view type
     */
    @Override
    public int getItemViewType(final int position) {
        MessageAndUser message = mMessagesAndUsers.get(position);

        if (message == null) {
            return ITEM_NULL;
        } else if (mMessagesAndUsers.get(position).message.userId == 1) {
            return ITEM_SENT_MESSAGE;
        }

        return ITEM_RECEIVED_MESSAGE;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == ITEM_NULL) {
            return new LoadingViewHolder(inflater.inflate(R.layout.rv_item_loading, parent, false));
        } else if (viewType == ITEM_SENT_MESSAGE) {
            return new SentMessageViewHolder(inflater.inflate(R.layout.rv_item_sent_message, parent, false));
        } else {
            return new ReceivedMessageViewHolder(inflater.inflate(R.layout.rv_item_received_message, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MessageAndUser messageAndUser = mMessagesAndUsers.get(position);

        //configures loading view
        if (holder instanceof LoadingViewHolder) {
            ((LoadingViewHolder) holder).mProgressBar.setIndeterminate(true);
        } else if (holder instanceof SentMessageViewHolder) {

            //configures sent message
            SentMessageViewHolder sentMessageViewHolder = (SentMessageViewHolder) holder;
            sentMessageViewHolder.setData(messageAndUser);

        } else if (holder instanceof ReceivedMessageViewHolder) {

            //configures received message
            ReceivedMessageViewHolder receivedMessageViewHolder = (ReceivedMessageViewHolder) holder;
            receivedMessageViewHolder.setData(messageAndUser);
        }
    }

    @Override
    public int getItemCount() {
        return mMessagesAndUsers != null ? mMessagesAndUsers.size() : 0;
    }

    private static class LoadingViewHolder extends RecyclerView.ViewHolder {

        private ProgressBar mProgressBar;

        LoadingViewHolder(View itemView) {
            super(itemView);
            mProgressBar = itemView.findViewById(R.id.pb_loading);
        }
    }

    private class SentMessageViewHolder extends RecyclerView.ViewHolder {

        private final TextView mMessage;
        private final AttachmentsView mAttachmentsView;

        SentMessageViewHolder(View itemView) {
            super(itemView);

            mAttachmentsView = itemView.findViewById(R.id.mav_attachments);
            mMessage = itemView.findViewById(R.id.tv_message_text);
        }

        void setData(final MessageAndUser messageAndUser) {
            itemView.setOnLongClickListener(__ -> onLongClickListener.onLongClickPerformedOnItem(messageAndUser.message.id, null, itemView));
            mMessage.setText(messageAndUser.message.content);

            //display the attachments if the message has any
            if (messageAndUser.message.attachments != null) {
                mAttachmentsView.setAttachments(messageAndUser, onAttachmentClickedListener, onLongClickListener);
            }
        }
    }

    private class ReceivedMessageViewHolder extends RecyclerView.ViewHolder {

        private final ImageView mUserPhoto;
        private final TextView mUserName;
        private final TextView mMessage;
        private final AttachmentsView mAttachmentsView;

        ReceivedMessageViewHolder(View itemView) {
            super(itemView);

            mAttachmentsView = itemView.findViewById(R.id.mav_attachments);
            mUserPhoto = itemView.findViewById(R.id.iv_message_user_photo);
            mUserName = itemView.findViewById(R.id.tv_message_name);
            mMessage = itemView.findViewById(R.id.tv_message_text);
        }

        void setData(final MessageAndUser messageAndUser) {

            itemView.setOnLongClickListener(__ -> onLongClickListener.onLongClickPerformedOnItem(messageAndUser.message.id, null, itemView));
            Picasso.get().load(messageAndUser.user.avatarId).transform(new CropCircleTransformation()).into(mUserPhoto);

            mUserName.setText(messageAndUser.user.name);
            mMessage.setText(messageAndUser.message.content);

            //display the attachments if the message has any
            if (messageAndUser.message.attachments != null) {
                mAttachmentsView.setAttachments(messageAndUser, onAttachmentClickedListener, onLongClickListener);
            }
        }
    }
}