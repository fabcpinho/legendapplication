package com.fpapps.legendchat.chat.model.pojo;

public class MessageAndUser {
    public MessagePojo message;
    public UserPojo user;

    public MessageAndUser(MessagePojo message, UserPojo user) {
        this.message = message;
        this.user = user;
    }
}
