package com.fpapps.legendchat.chat.presenter.contract;

import com.fpapps.legendchat.chat.model.pojo.MessageAndUser;

import java.util.ArrayList;

public interface RequestPresenterInteractorContract {

    interface Presenter{
        /**
         * Gets the requested list of messages returned from the interactor and deliver the
         * results to the view
         *
         * @param messages list of message and user
         */
        void messages(ArrayList<MessageAndUser> messages);

        /**
         * Failed getting info from API, needs to launch error message
         *
         * @param errorType error type
         */
        void failedFetchingMessages(int errorType);

        /**
         * Success on delete message
         * @param messageId of the message to delete
         */
        void successfullyDeletedMessage(int messageId);
    }

    interface Interactor{

        /**
         * Deletes message with the id passed as parameter
         * @param messageId id of the message to delete
         */
        void deleteMessageWithId(int messageId);

        /**
         * Gets messages and users from the network or database
         * @param numMessages number of messages to get
         * @param offset
         */
        void getMessagesAndUsers(int numMessages, int offset);

        /**
         * Dispose all the subscribed observables
         */
        void dispose();
    }


}
