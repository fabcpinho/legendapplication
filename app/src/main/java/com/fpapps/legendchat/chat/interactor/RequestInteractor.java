package com.fpapps.legendchat.chat.interactor;

import com.fpapps.legendchat.chat.model.pojo.MessageAndUser;
import com.fpapps.legendchat.chat.model.pojo.MessagePojo;
import com.fpapps.legendchat.chat.model.pojo.RequestPojo;
import com.fpapps.legendchat.chat.model.pojo.UserPojo;
import com.fpapps.legendchat.chat.model.repository.RequestRepository;
import com.fpapps.legendchat.chat.presenter.contract.RequestPresenterInteractorContract;
import com.fpapps.legendchat.rxjava.RxSchedulers;

import java.util.ArrayList;

import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class RequestInteractor implements RequestPresenterInteractorContract.Interactor {

    private RxSchedulers rxSchedulers;
    private RequestPresenterInteractorContract.Presenter requestPresenterContract;
    private RequestRepository requestRepository;


    private CompositeDisposable mDisposables;

    public RequestInteractor(RxSchedulers rxSchedulers, RequestPresenterInteractorContract.Presenter requestPresenterContract, RequestRepository requestRepository) {
        this.rxSchedulers = rxSchedulers;
        this.requestPresenterContract = requestPresenterContract;
        this.requestRepository = requestRepository;

        mDisposables = new CompositeDisposable();
    }

    private void fetchDataFromApi(int numMessages, int offset) {
        mDisposables.add(fetchDataFromApi().subscribe(requestPojo -> {
            // Persist messages in database
            updateDatabase(requestPojo);
            //Try to get messages from DB again after updating database
            getMessagesAndUsers(numMessages, offset);
        }, throwable -> requestPresenterContract.failedFetchingMessages(RequestRepository.ERROR_LOADING_FROM_NETWORK)));
    }

    private Maybe<RequestPojo> fetchDataFromApi() {
        return requestRepository.getNetworkMessages()
                .map(Response::body)
                .filter(requestPojo -> requestPojo != null)
                .compose(new RxSchedulers().applyMaybeSchedulerTransformer());
    }

    private void updateDatabase(RequestPojo requestPojo) {
        mDisposables.add(Single.just(requestPojo).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).subscribe(request -> {
            requestRepository.insertUsersInDatabase(requestPojo.users);
            requestRepository.insertMessagesInDatabase(requestPojo.messages);
        }));
    }

    @Override
    public void deleteMessageWithId(int messageId) {
        mDisposables.add(Single.create(emitter -> emitter.onSuccess(requestRepository.deleteById(messageId)))
                .compose(rxSchedulers.applySingleSchedulerTransformer())
                .subscribe(__ -> requestPresenterContract.successfullyDeletedMessage(messageId)));
    }

    /**
     * This method abstracts the presenter from how to get the data. The presenter only knows that
     * wants messages, and the interactor handles all the logic.
     * <p>
     * Tries to obtain it first from the database; If can't find any message locally, make an API
     * request to fill the database.
     *
     * @param numMessages number of messages to load
     * @param offset      number of messages already loaded to the view
     */
    @Override
    public void getMessagesAndUsers(int numMessages, int offset) {
        mDisposables.add(requestRepository.getDatabaseMessages(numMessages, offset)
                .map(messages -> {
                    if (messages.size() == 0) {
                        fetchDataFromApi(numMessages, offset);
                        throw new Exception();
                    }

                    ArrayList<MessageAndUser> messageAndUsers = new ArrayList<>();
                    for (MessagePojo msg : messages) {
                        UserPojo user = requestRepository.getUserById(msg.userId);
                        messageAndUsers.add(new MessageAndUser(msg, user));
                    }
                    return messageAndUsers;
                })
                .compose(rxSchedulers.applySingleSchedulerTransformer())
                .subscribe(messages -> {
                    requestPresenterContract.messages(messages);
                }, throwable -> requestPresenterContract.failedFetchingMessages(RequestRepository.ERROR_LOADING_FROM_DATABASE)));
    }

    @Override
    public void dispose() {
        mDisposables.dispose();
    }
}
