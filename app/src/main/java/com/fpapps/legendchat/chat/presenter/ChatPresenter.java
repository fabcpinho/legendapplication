package com.fpapps.legendchat.chat.presenter;

import com.fpapps.legendchat.chat.interactor.RequestInteractor;
import com.fpapps.legendchat.chat.model.pojo.MessageAndUser;
import com.fpapps.legendchat.chat.model.repository.RequestRepository;
import com.fpapps.legendchat.chat.presenter.contract.RequestPresenterInteractorContract;
import com.fpapps.legendchat.chat.view.contract.RequestViewPresenterContract;
import com.fpapps.legendchat.rxjava.RxSchedulers;

import java.util.ArrayList;

public class ChatPresenter implements RequestPresenterInteractorContract.Presenter, RequestViewPresenterContract.Presenter {
    private final RequestPresenterInteractorContract.Interactor requestInteractor;
    private RequestViewPresenterContract.View view;
    private final static int MESSAGE_CHUNK_NUMBER = 20;

    public ChatPresenter(RequestViewPresenterContract.View view, RequestRepository requestRepository, RxSchedulers rxSchedulers) {
        this.view = view;
        requestInteractor = new RequestInteractor(rxSchedulers, this, requestRepository);
    }

    @Override
    public void getMessages(int offset) {
        requestInteractor.getMessagesAndUsers(MESSAGE_CHUNK_NUMBER, offset * MESSAGE_CHUNK_NUMBER);
    }

    @Override
    public void messages(ArrayList<MessageAndUser> messages) {
        view.getMessagesResult(messages);
    }


    @Override
    public void failedFetchingMessages(int errorType) {
        view.getMessagesError(errorType);
    }


    @Override
    public void successfullyDeletedMessage(int messageId) {
        view.notifyAdapterItemRemoved(messageId);
    }

    @Override
    public void deleteMessage(int messageId) {
        requestInteractor.deleteMessageWithId(messageId);
    }

    @Override
    public void dispose() {
        requestInteractor.dispose();
    }
}
