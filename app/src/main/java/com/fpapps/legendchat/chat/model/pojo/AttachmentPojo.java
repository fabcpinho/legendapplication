package com.fpapps.legendchat.chat.model.pojo;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "attachment")
public class AttachmentPojo {
    @JsonProperty("id")
    @PrimaryKey
    @ColumnInfo(name = "id")
    @NonNull
    public String id;

    @JsonProperty("title")
    @ColumnInfo(name = "title")
    public String title;

    @JsonProperty("url")
    @ColumnInfo(name = "url")
    public String url;

    @JsonProperty("thumbnailUrl")
    @ColumnInfo(name = "thumbnailUrl")
    public String thumbnailUrl;
}
