package com.fpapps.legendchat.chat.model.pojo;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fpapps.legendchat.database.AttachmentConverter;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@TypeConverters({AttachmentConverter.class})
@Entity(tableName = "message", foreignKeys = @ForeignKey(entity = UserPojo.class, parentColumns = "id", childColumns = "userId"))
public class MessagePojo {
    @PrimaryKey
    @JsonProperty("id")
    @ColumnInfo(name = "id")
    public int id;

    @JsonProperty("userId")
    @ColumnInfo(name = "userId")
    public int userId;

    @JsonProperty("content")
    @ColumnInfo(name = "content")
    public String content;

    @JsonProperty("attachments")
    @ColumnInfo(name = "attachments")
    public List<AttachmentPojo> attachments;
}
