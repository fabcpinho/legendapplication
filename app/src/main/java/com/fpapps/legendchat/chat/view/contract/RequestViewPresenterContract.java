package com.fpapps.legendchat.chat.view.contract;

import com.fpapps.legendchat.chat.model.pojo.MessageAndUser;

import java.util.ArrayList;

/**
 * Interface between View and Presenter
 */
public interface RequestViewPresenterContract {

    /**
     * View interface to communicate with the presenter
     */
    interface View {
        /**
         * Messages are posted here by the presenter
         * @param messages list of {@link MessageAndUser}
         */
        void getMessagesResult(ArrayList<MessageAndUser> messages);

        /**
         * Error method in case of failure getting the messages
         * @param errorType throwable error message
         */
        void getMessagesError(int errorType);

        /**
         * Item removed successfully
         * @param messageId message id of the removed message
         */
        void notifyAdapterItemRemoved(int messageId);
    }

    /**
     * Presenter interface to communicate with the view
     */
    interface Presenter {
        /**
         * Get messages with offset.
         * @param offset paging offset
         */
        void getMessages(int offset);

        /**
         * Delete message by id
         * @param messageId id of the message to delete
         */
        void deleteMessage(int messageId);

        /**
         * Dispose method instructs presenter that the view is about to be destroyed.
         * Presenter should clean all the disposable objects.
         */
        void dispose();
    }

}
