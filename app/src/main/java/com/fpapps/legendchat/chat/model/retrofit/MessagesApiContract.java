package com.fpapps.legendchat.chat.model.retrofit;

import com.fpapps.legendchat.chat.model.pojo.RequestPojo;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.GET;

public interface MessagesApiContract {
    @GET("conversation")
    Single<Response<RequestPojo>> getMessages();
}
