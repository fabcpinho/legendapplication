package com.fpapps.legendchat.chat.model.repository;

import com.fpapps.legendchat.chat.model.pojo.MessagePojo;
import com.fpapps.legendchat.chat.model.pojo.RequestPojo;
import com.fpapps.legendchat.chat.model.pojo.UserPojo;
import com.fpapps.legendchat.chat.model.retrofit.MessagesApiContract;
import com.fpapps.legendchat.database.ApplicationDatabase;
import com.fpapps.legendchat.database.MessageDao;
import com.fpapps.legendchat.database.UserDao;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Response;

public class RequestRepository {
    public final static int ERROR_LOADING_FROM_NETWORK = 0;
    public final static int ERROR_LOADING_FROM_DATABASE = 1;

    private UserDao userDao;
    private MessageDao messageDao;
    private MessagesApiContract messagesApiContract;

    public RequestRepository(ApplicationDatabase applicationDatabase, MessagesApiContract messagesApiContract) {
        this.messagesApiContract = messagesApiContract;

        userDao = applicationDatabase.userDao();
        messageDao = applicationDatabase.messageDao();
    }

    public Single<Response<RequestPojo>> getNetworkMessages() {
        return messagesApiContract.getMessages();
    }

    public Single<List<MessagePojo>> getDatabaseMessages(int numMessages, int offset) {
        return messageDao.getMessages(numMessages, offset);
    }

    public void insertMessagesInDatabase(List<MessagePojo> messageList) {
        messageDao.insertMessages(messageList);
    }

    public int deleteById(long messageId) {
        return messageDao.deleteById(messageId);
    }

    public UserPojo getUserById(long userId) {
        return userDao.getUserWithIdSyncronously(userId);
    }

    public void insertUsersInDatabase(List<UserPojo> users) {
        userDao.insertUsers(users);
    }
}
