package com.fpapps.legendchat.splash.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.fpapps.legendchat.R;
import com.fpapps.legendchat.chat.model.pojo.MessageAndUser;
import com.fpapps.legendchat.chat.model.repository.RequestRepository;
import com.fpapps.legendchat.chat.presenter.ChatPresenter;
import com.fpapps.legendchat.chat.view.ChatActivity;
import com.fpapps.legendchat.chat.view.contract.RequestViewPresenterContract;
import com.fpapps.legendchat.dagger.component.DaggerSplashScreenComponent;
import com.fpapps.legendchat.dagger.module.ApplicationDatabaseModule;
import com.fpapps.legendchat.dagger.module.ApplicationModule;
import com.fpapps.legendchat.dagger.module.ChatPresenterModule;
import com.fpapps.legendchat.dagger.module.MessagesApiContractModule;
import com.fpapps.legendchat.database.ApplicationDatabase;

import java.util.ArrayList;

import javax.inject.Inject;

public class SplashScreenActivity extends Activity implements RequestViewPresenterContract.View {
    @Inject
    ChatPresenter chatPresenter;
    @Inject
    ApplicationDatabase appDatabase;

    private boolean animationEnded = false;
    private boolean messagesLoaded = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        handleInjection();

        startAnimations();

        if (savedInstanceState == null) {
            chatPresenter.getMessages(0);
        }
    }

    private void handleInjection() {
        DaggerSplashScreenComponent.builder()
                .messagesApiContractModule(new MessagesApiContractModule())
                .applicationDatabaseModule(new ApplicationDatabaseModule())
                .applicationModule(new ApplicationModule(this))
                .chatPresenterModule(new ChatPresenterModule(this))
                .build()
                .inject(this);
    }

    private void startAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.splash_screen_animation);
        anim.reset();
        LinearLayout l = findViewById(R.id.lin_lay);
        l.clearAnimation();
        l.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.splash_screen_translation);
        anim.reset();

        LinearLayout iv = findViewById(R.id.logo);
        iv.setVisibility(View.VISIBLE);
        iv.clearAnimation();
        iv.startAnimation(anim);

        iv.getAnimation().setAnimationListener(getSplashAnimationListener());
    }

    @NonNull
    private Animation.AnimationListener getSplashAnimationListener() {
        return new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                // Do nothing
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                animationEnded = true;
                startNextActivity();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // Do nothing
            }
        };
    }

    private void startNextActivity() {
        if (animationEnded && messagesLoaded) {
            Intent intent = new Intent(this, ChatActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void getMessagesResult(ArrayList<MessageAndUser> messages) {
        messagesLoaded = true;
        startNextActivity();
    }

    @Override
    public void getMessagesError(int errorType) {
        if (errorType == RequestRepository.ERROR_LOADING_FROM_NETWORK) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.no_internet_tittle)
                    .setMessage(R.string.no_internet_message)
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.try_again), (dialogInterface, i) -> {
                        chatPresenter.getMessages(0);
                        dialogInterface.dismiss();
                    })
                    .show();
        }
    }

    @Override
    public void notifyAdapterItemRemoved(int messageId) {
        //Do nothing
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        chatPresenter.dispose();
    }
}
