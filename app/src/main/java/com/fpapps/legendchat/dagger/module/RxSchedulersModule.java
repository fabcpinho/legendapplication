package com.fpapps.legendchat.dagger.module;

import com.fpapps.legendchat.rxjava.RxSchedulers;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RxSchedulersModule {
    @Provides
    @Singleton
    public RxSchedulers providesRxSchedulers(){
        return new RxSchedulers();
    }
}
