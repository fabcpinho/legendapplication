package com.fpapps.legendchat.dagger.module;

import com.fpapps.legendchat.chat.model.repository.RequestRepository;
import com.fpapps.legendchat.chat.presenter.ChatPresenter;
import com.fpapps.legendchat.chat.view.contract.RequestViewPresenterContract;
import com.fpapps.legendchat.rxjava.RxSchedulers;

import dagger.Module;
import dagger.Provides;

@Module(includes = {RequestRepositoryModule.class, RxSchedulersModule.class})
public class ChatPresenterModule {

    private RequestViewPresenterContract.View view;

    public ChatPresenterModule(RequestViewPresenterContract.View view) {
        this.view = view;
    }

    @Provides
    ChatPresenter providesChatPresenter(RxSchedulers rxSchedulers, RequestRepository requestRepository) {
        return new ChatPresenter(view, requestRepository, rxSchedulers);
    }
}
