package com.fpapps.legendchat.dagger.component;

import com.fpapps.legendchat.dagger.module.ApplicationDatabaseModule;
import com.fpapps.legendchat.dagger.module.ChatPresenterModule;
import com.fpapps.legendchat.splash.view.SplashScreenActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        ApplicationDatabaseModule.class,
        ChatPresenterModule.class

})
public interface SplashScreenComponent {

    void inject(SplashScreenActivity splashScreenActivity);
}
