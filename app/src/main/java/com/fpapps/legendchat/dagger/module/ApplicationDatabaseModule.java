package com.fpapps.legendchat.dagger.module;

import android.content.Context;

import com.fpapps.legendchat.database.ApplicationDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = ApplicationModule.class)
public class ApplicationDatabaseModule {
    @Provides
    @Singleton
    ApplicationDatabase getApplicationDatabase(Context context){
        return ApplicationDatabase.getAppDatabase(context);
    }
}
