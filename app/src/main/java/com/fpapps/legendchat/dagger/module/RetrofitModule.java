package com.fpapps.legendchat.dagger.module;

import com.fpapps.legendchat.api.RetrofitHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module(includes = RetrofitHelperModule.class)
public class RetrofitModule {
    private String url = "https://private-96848-7egend.apiary-mock.com";

    @Provides
    @Singleton
    public Retrofit providesRetrofit(RetrofitHelper retrofitHelper) {
        return retrofitHelper.configureRetrofitBuilder(url);
    }
}
