package com.fpapps.legendchat.dagger.component;

import com.fpapps.legendchat.chat.view.ChatActivity;
import com.fpapps.legendchat.dagger.module.ChatPresenterModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ChatPresenterModule.class})
public interface ChatActivityComponent {
    void inject(ChatActivity chatActivity);
}
