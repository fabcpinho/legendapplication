package com.fpapps.legendchat.dagger.module;

import com.fpapps.legendchat.chat.model.repository.RequestRepository;
import com.fpapps.legendchat.chat.model.retrofit.MessagesApiContract;
import com.fpapps.legendchat.database.ApplicationDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {ApplicationDatabaseModule.class, MessagesApiContractModule.class})
public class RequestRepositoryModule {
    @Provides
    @Singleton
    public RequestRepository providesRequestRepository(ApplicationDatabase applicationDatabase, MessagesApiContract messagesApiContract){
        return new RequestRepository(applicationDatabase, messagesApiContract);
    }
}
