package com.fpapps.legendchat.dagger.module;


import com.fpapps.legendchat.api.RetrofitHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RetrofitHelperModule {

  @Singleton
  @Provides
  public RetrofitHelper retrofitHelper(){
    return new RetrofitHelper();
  }
}
