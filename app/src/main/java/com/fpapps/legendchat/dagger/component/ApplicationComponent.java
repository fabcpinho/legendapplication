package com.fpapps.legendchat.dagger.component;

import android.content.Context;

import com.fpapps.legendchat.dagger.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        ApplicationModule.class

})
public interface ApplicationComponent {

    Context getContext();
}
