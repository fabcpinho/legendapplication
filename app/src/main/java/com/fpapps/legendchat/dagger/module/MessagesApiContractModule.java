package com.fpapps.legendchat.dagger.module;

import com.fpapps.legendchat.api.RetrofitHelper;
import com.fpapps.legendchat.chat.model.retrofit.MessagesApiContract;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module(includes = {RetrofitHelperModule.class, RetrofitModule.class})
public class MessagesApiContractModule {
    @Provides
    @Singleton
    MessagesApiContract getMessagesApiContract(RetrofitHelper retrofitHelper, Retrofit retrofit) {
        return retrofitHelper.makeARestMessagesService(retrofit);
    }
}
