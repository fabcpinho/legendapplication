package com.fpapps.legendchat.database;

import android.arch.persistence.room.TypeConverter;
import android.text.TextUtils;

import com.fpapps.legendchat.chat.model.pojo.AttachmentPojo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class AttachmentConverter {

    @TypeConverter
    public static List<AttachmentPojo> fromString(String attachments) {

        //if there are no attachments
        if (TextUtils.isEmpty(attachments)) {
            return new ArrayList<>();
        }

        Gson gson = new Gson();
        Type type = new TypeToken<List<AttachmentPojo>>() {}.getType();
        return gson.fromJson(attachments, type);
    }

    @TypeConverter
    public static String toString(List<AttachmentPojo> attachments) {
        Gson gson = new Gson();
        String str = gson.toJson(attachments);

        if (TextUtils.isEmpty(str) || str.equals("null")) {
            return "";
        }

        return str;
    }

}
