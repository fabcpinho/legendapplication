package com.fpapps.legendchat.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.fpapps.legendchat.chat.model.pojo.MessagePojo;

import java.util.List;

import io.reactivex.Single;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;


@Dao
public interface MessageDao {
    @Query("SELECT * FROM message LIMIT :limit OFFSET :offset")
    Single<List<MessagePojo>> getMessages(int limit, int offset);

    @Insert(onConflict = REPLACE)
    void insertMessages(List<MessagePojo> messages);

    @Query("DELETE FROM message WHERE id = :messageId")
    int deleteById(long messageId);
}
