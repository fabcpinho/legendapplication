package com.fpapps.legendchat.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.fpapps.legendchat.chat.model.pojo.AttachmentPojo;
import com.fpapps.legendchat.chat.model.pojo.MessagePojo;
import com.fpapps.legendchat.chat.model.pojo.UserPojo;


@Database(entities = {UserPojo.class, MessagePojo.class, AttachmentPojo.class}, version = 1)
public abstract class ApplicationDatabase extends RoomDatabase {

    private static ApplicationDatabase mAppDatabase;

    public static ApplicationDatabase getAppDatabase(Context context) {
        if (mAppDatabase == null) {
            mAppDatabase = Room.databaseBuilder(context, ApplicationDatabase.class, "db").build();
        }

        return mAppDatabase;
    }

    public abstract UserDao userDao();

    public abstract MessageDao messageDao();

}