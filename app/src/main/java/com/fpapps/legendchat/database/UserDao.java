package com.fpapps.legendchat.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.fpapps.legendchat.chat.model.pojo.UserPojo;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;


@Dao
public interface UserDao {
    @Query("SELECT * FROM user WHERE id LIKE :id")
    UserPojo getUserWithIdSyncronously(long id);

    @Insert(onConflict = REPLACE)
    void insertUsers(List<UserPojo> users);
}
