package com.fpapps.legendchat.views.custom;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.fpapps.legendchat.R;
import com.fpapps.legendchat.chat.adapters.ChatRecyclerViewAdapter;
import com.fpapps.legendchat.chat.model.pojo.AttachmentPojo;
import com.squareup.picasso.Picasso;


public class AttachmentCardView extends CardView {

    private ImageView mAttachmentThumbnailImage;
    private TextView mAttachmentTitle;

    public AttachmentCardView(final Context context) {
        super(context);
        init();
    }

    public AttachmentCardView(final Context context, @Nullable final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AttachmentCardView(final Context context, @Nullable final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

        removeAllViews();

        inflate(getContext(), R.layout.item_message_attachment, this);

        mAttachmentTitle = findViewById(R.id.tv_attachment_title_cv);
        mAttachmentThumbnailImage = findViewById(R.id.iv_attachment_thumbnail);
    }

    public void setAttachment(AttachmentPojo attachment, String userName, int messageId,
                              ChatRecyclerViewAdapter.OnAttachmentClickedListener onAttachmentClickedListener,
                              ChatRecyclerViewAdapter.OnLongClickListener onLongClickListener) {

        mAttachmentTitle.setText(attachment.title);

        Picasso.get().load(attachment.thumbnailUrl).into(mAttachmentThumbnailImage);

        setOnClickListener(__ -> onAttachmentClickedListener.onAttachmentClicked(userName, messageId, attachment, mAttachmentThumbnailImage));
        setOnLongClickListener(__ -> onLongClickListener.onLongClickPerformedOnItem(messageId, attachment, this));
    }
}
