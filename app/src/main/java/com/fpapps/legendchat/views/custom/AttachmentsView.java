package com.fpapps.legendchat.views.custom;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.fpapps.legendchat.chat.adapters.ChatRecyclerViewAdapter;
import com.fpapps.legendchat.chat.model.pojo.AttachmentPojo;
import com.fpapps.legendchat.chat.model.pojo.MessageAndUser;

public class AttachmentsView extends LinearLayout {

    public AttachmentsView(final Context context) {
        super(context);
    }

    public AttachmentsView(final Context context, @Nullable final AttributeSet attrs) {
        super(context, attrs);
    }

    public AttachmentsView(final Context context, @Nullable final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setAttachments(MessageAndUser messageAndUser, ChatRecyclerViewAdapter.OnAttachmentClickedListener onAttachmentClickedListener,
                               ChatRecyclerViewAdapter.OnLongClickListener onLongClickListener) {

        removeAllViews();

        for (AttachmentPojo attachment : messageAndUser.message.attachments) {
            AttachmentCardView attachmentCardView = new AttachmentCardView(getContext());
            attachmentCardView.setAttachment(attachment, messageAndUser.user.name, messageAndUser.message.id, onAttachmentClickedListener, onLongClickListener);
            addView(attachmentCardView);
        }
    }
}
